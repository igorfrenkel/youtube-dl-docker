# youtube-dl dockerfile

## Useful args

### Get mp3 from given video $1=name, $2 url

--extract-audio --audio-format mp3 --output "$1.%(ext)s" $2
