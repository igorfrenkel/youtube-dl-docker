FROM debian:latest

RUN apt-get update && apt-get install -y ffmpeg python3 curl && \
    ln -s /usr/bin/python3.7 /usr/bin/python

RUN curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl && \
    chmod u+x /usr/local/bin/youtube-dl

ENTRYPOINT ["/usr/local/bin/youtube-dl"]
CMD ["--help"]
